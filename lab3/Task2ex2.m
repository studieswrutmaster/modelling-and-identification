clc
clear all
close all

lambda = 1;

mu = lambda;
sigma = lambda^2;

n = 1000;

Y_vect = [];

for k = 1:1000
    X = exprnd(lambda,n,1);
    Y = (sum(X)-n*mu)/(sigma*sqrt(n));
    Y_vect = [Y_vect Y];
end

figure
subplot(2,1,1)
hist(Y_vect)
xlim([-10 10])

subplot(2,1,2)

x = -10:0.002:10;

pdf = normpdf(x,0,1);
xlim([-10 10])
plot(x,pdf,'Color','red','LineWidth',2)