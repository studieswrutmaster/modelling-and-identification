clc
clear all
close all

% mu = 0;
% sigma = 1;

Y_vect = [];


% lambda = 1;
% 
% mu = lambda;
% sigma = lambda^2;


% A = 3;
% B = 2;
% 
% mu = A/(A+B);
% sigma = A*B/((A+B)^2*(A+B+1));

a = 0;
b = 1;

mu = (a+b)/2;
sigma = (b-a)^2/12;

for n = logspace(2,10,9)
    for k = 1:100
%         X = normrnd(mu,sigma,n,1);
%         X = exprnd(lambda,n,1);
%         X = betarnd(A,B,n,1);
        X = rand(n,1);
        
        Y = (sum(X)-n*mu)/(sigma*sqrt(n));
        Y_vect = [Y_vect Y];
    end
        
    figure(1)
    subplot(2,1,1)
    hist(Y_vect)
    title(sprintf('n = %d',n))
    xlim([-10 10])
%     hold on

    subplot(2,1,2)

    x = -10:0.002:10;

    pdf = normpdf(x,0,1);
    xlim([-10 10])
    plot(x,pdf,'Color','red','LineWidth',2)   
%     hold on
    pause(1)
end
