clc
clear all
close all


a = 0;
b = 1;

mu = (a+b)/2;
sigma = (b-a)^2/12;

M = [];
n_vect = [];
V = [];

for n = logspace(1,10,1000)
    nint = uint16(n);
    % generate random sample
    R = rand(nint,1);

    % calculate mean
    M = [M mean(R)];
    n_vect = [n_vect nint];
    
    % calculate variance
    V = [V var(R)];
end

figure
subplot(2,1,1)
plot(n_vect,M,n_vect,ones(1,length(n_vect))*mu,'r')
grid
xlabel('n')
ylabel('mean')

subplot(2,1,2)
plot(n_vect,V,n_vect,ones(1,length(n_vect))*sigma,'r')
grid
xlabel('n')
ylabel('variance')