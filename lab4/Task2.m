clc
clear all
close all

n = 1000;

%% normal
X = [normrnd(0,1,n,1); normrnd(5,0.25,n,1); normrnd(15,1,n,1)];
x = linspace(-4,19,n);
pdf = (normpdf(x,0,1)+normpdf(x,5,0.25)+normpdf(x,15,1))/3; 

%% uniform
X = [(rand(n,1)*2);(rand(n,1)*0.5)+1;(rand(n,1)*1)+3];
x = linspace(-1,5,n);
pdf = (unifpdf(x,0,2)+unifpdf(x,1,1.5)+unifpdf(x,3,4))/3; 

%% beta
X = [4+betarnd(2,2,n,1); 5+5*betarnd(3,2,n,1); 6+betarnd(2,2,n,1)];
x = linspace(3,11,n);
pdf = (betapdf(x-4,2,2)+betapdf((x-5)/5,3,2)+betapdf((x-6),2,2))/3; 

%% vizualization
for bw = 0.1:0.1:2.0
    [f,xi,bw_out] = ksdensity(X,'bandwidth',bw,'npoints',n);
    plot(x,pdf,'r',xi,f,'b')
    title(sprintf('bw = %.1f',bw))
    axis([x(1) x(length(x)) 0 1])
    legend('pdf','estimation','Location','Best')
    pause(0.5)
end


% [fbox,xibox,bwbox] = ksdensity(X,'kernel','box');

% hold
% ksdensity(X);
% ksdensity(X,'kernel','box');
% ksdensity(X,'kernel','triangle')
% ksdensity(X,'kernel','epanechnikov')

% legend('theoretical','normal','box','triangle','epanechnikov')