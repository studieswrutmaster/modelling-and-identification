clc
clear all
close all

n = 100;

%% normal
X = [normrnd(0,1,n,1); normrnd(5,0.25,n,1); normrnd(15,1,n,1)];
x = linspace(-4,19,n);
pdf = (normpdf(x,0,1)+normpdf(x,5,0.25)+normpdf(x,15,1))/3; 
mu1 = 0;
mu2 = 5;
mu3 = 15;

%% uniform
% X = [(rand(n,1)*2);(rand(n,1)*0.5)+1;(rand(n,1)*1)+3];
% x = linspace(-1,5,n);
% pdf = (unifpdf(x,0,2)+unifpdf(x,1,1.5)+unifpdf(x,3,4))/3; 
% mu1 = 2/2;
% mu2 = 2.5/2;
% mu3 = 7/2;

%% beta
% X = [4+betarnd(2,2,n,1); 5+5*betarnd(3,2,n,1); 6+betarnd(2,2,n,1)];
% x = linspace(3,11,n);
% pdf = (betapdf(x-4,2,2)+betapdf((x-5)/5,3,2)+betapdf((x-6),2,2))/3;
% mu1 = (2/(2+2))+4;
% mu2 = (3/(3+2))*5+5;
% mu3 = (2/(2+2))+6;

figure
plot(x,pdf,'r')
hold 
plot(X,0,'b+')
plot(ones(25,1)*mu1,linspace(0,1,25),'g')
plot(ones(25,1)*mu2,linspace(0,1,25),'g')
plot(ones(25,1)*mu3,linspace(0,1,25),'g')

