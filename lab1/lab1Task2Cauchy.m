%% Modeling and Indentification lab 1 Task 2 Cauchy
clc
clear all
close all

%% Task 2
n = 10^5;

x0 = 0;
xF = 100;

mu = 10;
sigma = 1;

figure(1)

u = [];

title('Cauchy')
subplot(2,1,1)
U = rand(n,1);
for n = 1:n
   U = sigma.*tan(pi*(rand()-0.5))+mu;
   if U < xF && U > x0
   u = [u U];
   end
end
% u = tan(pi.*(U-0.5));
% u = sigma.*tan(pi*(U-0.5))+mu;
hist(u,100);
title(['Histogram for n = ',int2str(n)])
% xlim([0 100])

subplot(2,1,2)

x = x0:0.002:xF;

pdf = cauchypdf(x,mu,sigma);

plot(x,pdf,'Color','red','LineWidth',2)
title('Probability Density Function')