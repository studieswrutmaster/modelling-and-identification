%% Modeling and Indentification lab 1 Task 2 Logistic
clc
clear all
close all

%% Task 2
n = 10^6;

mu = 6;
sigma = 1;

figure(1)

title('Logistic')
subplot(2,1,1)
U = rand(n,1);
u =  mu + sigma*(log(U)-log(1-U));
hist(u,100);
xlim([0 10])
title(['Histogram for n = ',int2str(n)])

subplot(2,1,2)

x = 0:0.002:10;


pd = makedist('Logistic','mu',mu,'sigma',sigma);
pdf = pdf(pd,x);

plot(x,pdf,'Color','red','LineWidth',2)
title('Probability Density Function')