%% Modeling and Indentification lab 1 Task 2 Triangular
clc
clear all
close all

%% Task 2
n = 10^6;

a = 0;
c = 7;
b = c;

x_max = c;

figure(1)

title('Triangular')
subplot(2,1,1)
U = rand(n,1);
% u1 = a+sqrt(U*(b-a)*(c-a));%for 0 < U < F(c)
% u2 = b-sqrt((1-U)*(b-a)*(b-c));
% u = (u1 + u2)-1;
u = c*sqrt(U);
xlim([0 x_max])
hist(u,100);
title(['Histogram for n = ',int2str(n)])

subplot(2,1,2)

x = 0:0.002:x_max;


pd = makedist('Triangular','a',a,'b',b,'c',c);
pdf = pdf(pd,x);
xlim([0 x_max])
plot(x,pdf,'Color','red','LineWidth',2)
title('Probability Density Function')