%% Modeling and Indentification lab 1 Task 2 Exponential
clc
clear all
close all

%% Task 2
n = 10^6;

mu = 5;
x_max = 40;

figure(1)

title('Exponential')
subplot(2,1,1)
U = rand(n,1);
u = -(log(U)*mu);
hist(u,x_max*2);
xlim([0 x_max])
title(['Histogram for n = ',int2str(n)])

subplot(2,1,2)

x = 0:0.002:x_max;


pd = makedist('Exponential','mu',mu);
pdf = pdf(pd,x);

plot(x,pdf,'Color','red','LineWidth',2)
xlim([0 x_max])
title('Probability Density Function')