%% Modeling and Indentification lab 1 Task 1
clc
clear all
close all

%% Task 1
a = 2;
b = 6;
i = 1;
for n = logspace(a,b,b-a+1)
    figure(i)
    
    subplot(2,1,1)
    u = rand(n,1);
    hist(u,100)
    title(['Histogram for n = ',int2str(n)])

    subplot(2,1,2)
    x = -0.1:0.002:1.1;
    pdf = unifpdf(x);
    plot(x,pdf,'Color','red','LineWidth',2)
    xlim([-0.05 1.05])
    ylim([-0.2 1.2])
    title('Probability Density Function')
    i = i + 1;
end