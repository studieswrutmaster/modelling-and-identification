clc
clear all
close all
%% parameters
n = 10^5;
x_max = 5;


a = 0;
b = 1;

c = sqrt(2*exp(1)/pi);

%% generated random variable
figure(1)
subplot(2,1,1)

U = [];

for i = 0:n
    u = rand();
    x = randl(); 
    y = (c *exp(-abs(x)) * u)/2;
    l = normpdf(x,a,b);
    if y <= l
       U = [U x]; 
    end

end

hist(U,100);
xlim([-x_max x_max])
title(['Histogram for n = ',int2str(n)])

%% theoretical pdf 
subplot(2,1,2)
x = -x_max:0.002:x_max;
y = normpdf(x,a,b);

plot(x,y,'Color','red','LineWidth',2)
xlim([-x_max x_max])
title('Probability Density Function')