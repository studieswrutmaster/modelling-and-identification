clc
clear all
close all
%% parameters
n = 10^4;
x_max = 2;

c = 2;

%% ganerated radom variable
figure(1)
subplot(2,1,1)

U = [];

for i = 0:n
    u = rand();
    x = rand(); 
    y = c * pdf(makedist('Uniform'),x) * u;
    l = 2*(1-x);
    if y <= l
       U = [U x]; 
    end

end



hist(U);
xlim([0 x_max])
title(['Histogram for n = ',int2str(n)])

%% theoretical pdf 
subplot(2,1,2)
x = 0:0.002:1;
y = 2*(1-x);

plot(x,y,'Color','red','LineWidth',2)
xlim([0 x_max])
title('Probability Density Function')